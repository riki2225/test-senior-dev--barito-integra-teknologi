/**
 * Author : Riki Setiyo Pambudi
 * Email : Setiyoriki589@gmail.com
 */

function getProfitMaximum(prices) {
  //Define Array
  let minProfit = prices[0];
  let maxProfit = prices[1] - prices[0];
  let profit = Math.sign(maxProfit);

  for (let i = 1; i < prices.length; i++) {
    let currentPrice = prices[i];
    let potentialProfit = currentPrice - minProfit;
    maxProfit = Math.max(maxProfit, potentialProfit);
    minProfit = Math.min(minProfit, currentPrice);
  }

  if (profit == -1) {
    return "<tidak dapat membeli></tidak>";
  }

  return maxProfit;
}

var arr1 = [5, 6, 15, 3, 10, 22, 15];
var arr2 = [10, 15, 8, 7, 14];
var arr3 = [100, 90, 80, 75, 65];

console.log(getProfitMaximum(arr1));
console.log(getProfitMaximum(arr2));
console.log(getProfitMaximum(arr3));
