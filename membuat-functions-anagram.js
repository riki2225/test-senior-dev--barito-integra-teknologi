/*
 * Author : Riki Setiyo P.
 * Email : Setiyoriki589@gmail.com
 * Get anagram Function
 */

function anagramArray(parArr) {
  let returnResult = [];

  // check params array
  if (parArr == null || parArr.length == 0) return returnResult;

  // mapping array
  let mapParrams = {};
  for (let i = 0; i < parArr.length; i++) {
    let arr = parArr[i].split("");
    arr.sort();
    let t = arr.join("");
    if (mapParrams[t] == null) {
      let l = [];
      l.push(i);
      mapParrams[t] = l;
    } else {
      mapParrams[t].push(i);
    }
  }

  for (let l in mapParrams) {
    if (mapParrams[l].length > 1) {
      for (let i = 0; i < mapParrams[l].length; i++) {
        returnResult.push(parArr[mapParrams[l][i]]);
      }
    }
  }

  return returnResult;
}

console.log(anagramArray(["bat", "cod", "cat", "act", "cab", "crazy", "tac"]));
